import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule}from '@angular/router'

import{
  EventsListComponent,
  EventThumbnailComponent,
  EventService,
  EventDetailsComponent,
  CreateEventComponent,
  EventRouteActivator,
  EventListResolver,
  CreateSessionComponent,
  SessionListComponent,
  DurationPipe
}from './events/index'

import { EventsAppComponent } from './events-app.component';
import { NavBarComponent } from './nav/navbar.component';
import { CollapisableWellComponent } from './common/collapsible-well.component';
import { TOASTR_TOKEN ,Toastr} from './common/toastr.service';
import { appRoutes } from '../routes'
import { Error404Component } from './errors/404.component';
import {AuthService} from '../user/auth.service';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

let toastr:Toastr = window['toastr'];

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    EventDetailsComponent,
    CreateEventComponent,
    NavBarComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapisableWellComponent,
    DurationPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers:[
    EventService,
    {provide:TOASTR_TOKEN,useValue:toastr},
    EventRouteActivator,
    EventListResolver,
    AuthService,
  {
    provide:'canDeactivateCreateEvent',
    useValue:checkDirtyState
  }],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }
  export function checkDirtyState(component:CreateEventComponent){
    if (component.isDirty)
      return window.confirm('You have not saved this event, do you really want to cancel?')
    return true
  }

